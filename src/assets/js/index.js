$(document).ready(function(){
	$(document).on("click","a[href='#']",function(e){
		e.preventDefault();
	})

	var hotelJsonData = "./assets/json/hotel.json",
	clone;
	$.get(hotelJsonData , function( data ) {
		$(data.arrayListing).each(function(i,v){
			clone = $(".hotel-item").clone()[0];
			clone = $(clone)
			clone.removeClass("clone hidden");
			clone.find(".title").text(v.hotelName)
			clone.find(".score .customer-score").text(v.customerScore)
			clone.find(".score .review-count").text(v.reviewCount + " Değerlendirme")
			clone.find(".score .point-text").text(v.customersPointText)
			clone.find(".images img").attr("src",v.photoPath)
			clone.find(".hotel-map").text(v.areaName + " - " + v.subAreaName + (v.subAreaDetailName == null ? "" : " | " + v.subAreaDetailName) );
			clone.find(".erk-promo").text(v.campaignName)
			clone.find(".price-detail .hotel-type").text(v.accommodation)
			clone.find(".currency").html(v.price + "<small class='price-currency'> TL</small>")
			clone.find(".discount-price").html(v.discountPrice + "<small class='price-currency'> TL</small>")
			clone.find(".btn").attr("href", v.url)

			clone.find(".hotel-features-list").html("");
			$(v.hotelPropertyList).each(function(index,element){
				clone.find(".hotel-features-list").append('<span class="list-item">' + element.name +'</span>')
			})

			//*****hotelThemeList fazla data olması ve tasarımı bozması sebebi ile göstermedim.*******

			//clone.find(".badge-list").html("");
			//$(v.hotelThemeList).each(function(index,element){
			//	clone.find(".badge-list").append('<span class="item">' + element.text +'</span>')
			//})
			
			$(".hotel-listing").append(clone)

		})
		$(".hotel-item.clone").remove()
	});
});



